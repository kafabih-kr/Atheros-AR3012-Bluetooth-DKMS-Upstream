# Description

This bluetooth source is from Linux kernel upstream source tree

Attention! this is a DKMS module, so it need to install using DKMS.

If you use <a href="https://gitlab.com/kafabih-kr/Atheros-AR3012-Bluetooth-Fix-for-Linux-Kernel-4.15">this</a> driver above Linux 4.15, you should uninstall the Atheros-AR3012-Bluetooth-Fix-for-Linux-Kernel-4.15 driver

# Changelogs
20/June/2018
- Updated from upstream.

# Installation
1. Clone or download this repo, if you download it, you must extract first.
2. Add it to DKMS as below

   <code>sudo dkms add ./Atheros-AR3012-Bluetooth-DKMS-Upstream-master/</code>
3. Install it using DKMS as below

   <code>sudo dkms install bluetooth-ath3k-upstream/1.5 --force</code>
4. Reboot or restart your GNU/Linux machine to check are your Atheros AR3012 Bluetooth is functioned.

# Uninstall
1. Uninstall it using DKMS as below

   <code>sudo dkms remove bluetooth-ath3k-upstream/1.5 --all</code>
4. Reboot or restart your GNU/Linux machine.
